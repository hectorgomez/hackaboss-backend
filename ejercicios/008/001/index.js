// Servidor que escucha en localhost:3000 y tiene un endpoint
// POST /api/users/:userId
// que acepta un body con la estructura
// { email: 'email@dominio.com', premios: 14 }

// Este endpoint debe validar que:
// 1. El userId que se pasa como path param es obligatorio y es un numero mayor que 0
// 2. El email es obligatorio y está bien formado
// 2. El valor de premios es un número entre 0 y 100 y es opcional

// En caso de error, debería devolver el status 400 al cliente
// y como body un JSON con la estructura:
// { resultado: 'ERROR', error: $mensajeDeErrorDeJoi }

// Si no hay errores de validación debería devolver el status 201 al cliente
// y como body un JSON con la estructura:
// { resultado: 'OK' }

const express = require('express');
const Joi = require('joi');

const app = express();

app.use(express.json());

app.post('/api/users/:userId', async (req, res) => {
  try {
    const { userId } = req.params;
    const { email, premios } = req.body;

    const schema = Joi.object({
      userId: Joi.number().min(0).required(),
      email: Joi.string().email().required(),
      premios: Joi.number().min(0).max(100),
    });

    await schema.validateAsync({ userId, email, premios });

    res.send({ resultado: 'OK' });
  } catch (err) {

    // let status;

    // if (err.isJoi) {
    //   status = 400;
    // } else {
    //   status = 500;
    // }

    const status = err.isJoi ? 400 : 500;
    res.status(status);
    res.send({ resultado: 'ERROR', error: err.message });
  }
});

app.listen(3000, () => console.log('Servidor escuchando'));