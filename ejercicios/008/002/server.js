// PARTE 1:
// Servidor que escucha en localhost:3000 y tiene dos rutas
// /GET /api/users/:name -> devuelve el usuario con nombre === name
// /GET /api/users -> devuelve todos los users si no se manda edad en el querystring
// /GET /api/users -> si nos llega el querystring edad, devolvemos los users
// que tengan esa edad.

require('dotenv').config();

const fs = require('fs').promises;
const path = require('path');
const express = require('express');
const Joi = require('joi');
const multer = require('multer');
const { v4: uuidv4 } = require('uuid');

const { PORT } = process.env;

const app = express();

const upload = multer({
  storage: multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './images/')
    },
    filename: function (req, file, cb) {
      cb(null, uuidv4() + path.extname(file.originalname))
    }
  }),
  limits: {
    fileSize: 10 * 1024 * 1024, // 10 MB
  }
});

// const upload = multer({ dest: './images' });

app.use(express.json());

app.get('/api/users/:name', (req, res) => {
  const usersData = require('./users.json');
  const { name } = req.params;

  const user = usersData.usuarios.find(usuario => usuario.nombre === name);

  if (!user) {
    res.status(404);
    res.send();
  } else {
    res.send(user);
  }
});

app.get('/api/users', (req, res) => {
  const usersData = require('./users.json');
  const { edad } = req.query;

  if (edad) {
    const edadNumerica = Number(edad);
    const usuarios = usersData.usuarios.filter(usuario => usuario.edad === edadNumerica);
    res.send(usuarios);
  } else {
    res.send(usersData.usuarios);
  }
});

app.post('/api/users', async (req, res) => {

  try {
    const schema = Joi.object({
      nombre: Joi.string().required(),
      edad: Joi.number().min(18).required(),
    });

    await schema.validateAsync(req.body);

    const usersData = require('./users.json');
    usersData.usuarios.push(req.body);

    await fs.writeFile(path.join(__dirname, 'users.json'), JSON.stringify(usersData));

    res.send(req.body);

  } catch (err) {
    const status = err.isJoi ? 400 : 500;
    res.status(status);
    res.send({ resultado: 'ERROR', message: err.message });
  }
});

const validate = async (req, res, next) => {
  try {
    const { name } = req.params;
    const schema = Joi.string().required();
    await schema.validateAsync(name);

    const usersData = require('./users.json');
    const user = usersData.usuarios.find(usuario => usuario.nombre === name);

    if (!user) {
      const error = new Error('Usuario no encontrado');
      error.code = 404;

      throw error;
    }

    next();

  } catch (err) {
    // let status;

    // if (err.isJoi) {
    //   status = 400;
    // } else if (err.code) {
    //   status = err.code;
    // } else {
    //   status = 500;
    // }

    const status = err.isJoi ? 400 : (err.code || 500);
    res.status(status);
    res.send({ resultado: 'ERROR', message: err.message });
  }
}

const respond = (req, res) => {
  res.status(201);
  res.send({
    resultado: 'OK',
    imagen: req.file.filename,
  });
}

app.post('/api/users/:name/images', validate, upload.single('avatar'), respond);

app.listen(PORT, () => console.log(`Servidor escuchando en el puerto ${PORT}`));

// PARTE 2:
// 2.1 Crea una nueva colección en Postman que se llame
// express-users-api y que contenga las request que
// hemos creado en la parte 1.

// 2.1 Añade un nuevo endpoint que acepte peticiones POST 
// en /users con un body { nombre: $nombre, edad: $edad }
// y añada la info entrante a nuestro users.json y nos
// devuelva el usuario creado con un status code 201 CREATED.

// 2.2 Usa Joi para verificar que las request entrantes
// tienen el formato correcto. Ambas propiedades (nombre, edad)
// son obligatorias. Edad tiene que ser mayor que 18 años.

// 2.3 Crea un nuevo endpoint que acepte peticiones POST
// a /users/:name donde :name hace referencia a uno de los
// usuarios que tenemos en users.json y cuyo body tenga
// content-type multipart/form-data y contenga una imagen.
// Si el usuario existe, guardaremos la imagen en /images,
// y devolvemos 201 CREATED, con el objeto
// { resultado: 'OK', imagen: $nombreDelArchivoSubido }
// Si no existe, devolveremos un error 404 Not Found.


// Date.now() to name files

// const upload = multer({
//   storage: multer.diskStorage({
//     destination: function (req, file, cb) {
//       cb(null, './images/')
//     },
//     filename: function (req, file, cb) {
//       cb(null, Date.now() + path.extname(file.originalname))
//     }
//   }),
//   limits: {
//     fileSize: 10 * 1024 * 1024, // 10 MB
//   }
// });

// Date.now() to name files

// const upload = multer({
//   storage: multer.diskStorage({
//     destination: function (req, file, cb) {
//       cb(null, './images/')
//     },
//     filename: function (req, file, cb) {
//       cb(null, Date.now() + path.extname(file.originalname)) // https://www.npmjs.com/package/mime-types
//     }
//   }),
//   limits: {
//     fileSize: 10 * 1024 * 1024, // 10 MB
//   }
// });

// uuid to name files

// const { v4: uuidv4 } = require('uuid');

// const upload = multer({
//   storage: multer.diskStorage({
//     destination: function (req, file, cb) {
//       cb(null, './images/')
//     },
//     filename: function (req, file, cb) {
//       cb(null, uuidv4() + path.extname(file.originalname))
//     }
//   }),
//   limits: {
//     fileSize: 10 * 1024 * 1024, // 10 MB
//   }
// });
