const Joi = require('joi');

const { booksRepository, usersRepository } = require('../repositories');

// const booksRepository = require('../repositories/books-repository');
// const usersRepository = require('../repositories/users-repository');

async function getBooks(req, res, next) {
  try {
    const books = await booksRepository.findBooks();
    res.send(books);
  } catch (err) {
    next(err)
  }
}

async function addBook(req, res, next) {
  try {
    const { titulo, autor } = req.body;

    const schema = Joi.object({
      titulo: Joi.string().required(),
      autor: Joi.string().required(),
    });

    await schema.validateAsync({ titulo, autor });

    const book = await booksRepository.addBook({ titulo, autor });

    res.status(201);
    res.send(book);
  
  } catch (err) {
    next(err);
  }
}

async function updateBook(req, res, next) {
  try {
    const { id } = req.params;
    const { titulo, autor } = req.body;

    let schema = Joi.number().positive().required();
    await schema.validateAsync(id);

    schema = Joi.object({
      titulo: Joi.string().required(),
      autor: Joi.string().required(),
    });

    await schema.validateAsync({ titulo, autor });

    let book = await booksRepository.findBookById(id);
    checkExistBook(book);
    book = await booksRepository.updateBook(id, { titulo, autor });

    res.send(book);
  } catch (err) {
    next(err);
  }
}

async function assignBook(req, res, next) {
  try {
    const { id } = req.params;
    const { userId } = req.body;
  
    const schema = Joi.number().positive().required();
    await schema.validateAsync(userId);
  
    const book = await booksRepository.findBookById(id);
    checkExistBook(book);
    const user = await usersRepository.getUserById(userId);

    if (!user) {
      const err = new Error('Usuario no encontrado');
      err.code = 404;

      throw err;
    }

    if (book.user_id !== null) {
      const err = new Error('El libro ya estaba asignado');
      err.code = 400;

      throw err;
    }

    const assignedBook = await booksRepository.assignBook(id, userId);
    res.send(assignedBook);
  } catch (err) {
    next(err);
  }
}

async function unassignBook(req, res, next) {
  try {
    const { id } = req.params;

    const schema = Joi.number().positive().required();
    await schema.validateAsync(id);

    const book = await booksRepository.findBookById(id);
    checkExistBook(book);

    if (!book.user_id) {
      const err = new Error('El libro ya está libre');
      err.code = 400;

      throw err;
    }

    const unassignedBook = await booksRepository.unassignBook(id);
    res.send(unassignedBook);
  } catch (err) {
    next(err);
  }
}

async function deleteBook(req, res, next) {
  try {
    const { id } = req.params;

    const book = await booksRepository.findBookById(id);
    checkExistBook(book);

    if (book.user_id !== null) {
      const user = await usersRepository.getUserById(book.user_id);
      const err = new Error(`El libro ${book.titulo} está asignado al usuario ${user.nombre}, no se puede borrar`);
      err.code = 400;

      throw err;
    }

    await booksRepository.deleteBook(id);

    res.status(204);
    res.send();
  } catch (err) {
    next(err);
  }
}

const checkExistBook = (book) => {
  if (!book) {
    const err = new Error('Libro no encontrado');
    err.code = 404;

    throw err;
  }
}

module.exports = {
  addBook,
  getBooks,
  updateBook,
  assignBook,
  unassignBook,
  deleteBook,
};
