const Joi = require('joi');

const { usersRepository } = require('../repositories');
// const usersRepository = require('../repositories/users-repository');

async function getUsers(req, res, next) {
  try {
    const users = await usersRepository.getUsers();
    res.send(users);
  } catch (err) {
    next(err);
  }
}

async function getUserById(req, res, next) {
  try {
    const { id } = req.params;
    const schema = Joi.number().positive();
    await schema.validateAsync(id);
    
    const user = await usersRepository.getUserById(id);
    res.send(user);

  } catch (err) {
    next(err);
  }
}

module.exports = {
  getUsers,
  getUserById,
};
