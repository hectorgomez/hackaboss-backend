const { database } = require('../infrastructure');

const getUsers = async () => {
  const query = 'SELECT * FROM users';
  const [resultado] = await database.pool.query(query);

  return resultado;
};

const getUserById = async (id) => {
  const query = 'SELECT * FROM users WHERE id = ?';
  const [rows] = await database.pool.query(query, id);

  return rows[0];
};

module.exports = {
  getUsers,
  getUserById,
};
