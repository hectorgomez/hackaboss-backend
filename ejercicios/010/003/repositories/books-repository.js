const { database } = require('../infrastructure');

const findBooks = async () => {
  const query = 'SELECT * FROM books';
  const [books] = await database.pool.query(query);

  return books;
};

const findBookById = async (id) => {
  const query = 'SELECT * FROM books WHERE id = ?';
  const [books] = await database.pool.query(query, id);

  // return books && books[0];

  return books[0];
};

const addBook = async (data) => {
  const { titulo, autor } = data;

  const consulta = 'INSERT INTO books (titulo, autor) VALUES (?, ?)';
  const [result] = await database.pool.query(consulta, [titulo, autor]);

  return findBookById(result.insertId);
};

const updateBook = async (id, data) => {
  const query = 'UPDATE books SET autor = ?, titulo = ? WHERE id = ?';
  await database.pool.query(query, [data.autor, data.titulo, id]);

  return findBookById(id);
};

const assignBook = async (id, userId) => {
  const query = 'UPDATE books SET user_id = ? WHERE id = ?';
  await database.pool.query(query, [userId, id]);

  return findBookById(id);
};

const unassignBook = async (id) => {
  const query = 'UPDATE books SET user_id = null WHERE id = ?';
  await database.pool.query(query, id);

  return findBookById(id);
};

const deleteBook = async (id) => {
  const query = 'DELETE FROM books WHERE id = ?';

  return database.pool.query(query, id);
};

module.exports = {
  findBooks,
  addBook,
  findBookById,
  updateBook,
  assignBook,
  unassignBook,
  deleteBook,
};
