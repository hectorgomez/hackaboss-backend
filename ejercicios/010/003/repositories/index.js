const booksRepository = require('./books-repository');
const usersRepository = require('./users-repository');

module.exports = {
  booksRepository,
  usersRepository,
};
