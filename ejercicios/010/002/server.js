require('dotenv').config();
const express = require('express');

const { PORT } = process.env;

const { booksController, usersController } = require('./controllers');

const app = express();

app.use(express.json());

// Users 
app.get('/api/users', usersController.getUsers);
app.get('/api/users/:id', usersController.getUserById);

// Books
app.get('/api/books', booksController.getBooks);
app.post('/api/books', booksController.addBook);
app.put('/api/books/:id', booksController.updateBook);
app.patch('/api/books/:id/assign', booksController.assignBook);
app.patch('/api/books/:id/unassign', booksController.unassignBook);
app.delete('/api/books/:id', booksController.deleteBook);

app.listen(PORT,
  () => console.log(`Servidor escuchando en el puerto ${PORT}`));