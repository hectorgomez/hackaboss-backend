const handleError = (err, res) => {
  const status = err.isJoi ? 400 : (err.code || 500);
  res.status(status);
  res.send({ error: err.message });
};

module.exports = {
  handleError,
};