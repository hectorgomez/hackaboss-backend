const Joi = require('joi');
const { database } = require('../infrastructure');
const { handleError } = require('./utils/error-handler')

async function getBooks(req, res) {
  try {
    const query = 'SELECT * FROM books';
    const [rows] = await database.pool.query(query);
    res.send(rows);
  } catch (err) {
    handleError(err, res);
  }
}

// POST endpoint addBook que crea un libro.
// Recibirá un body con los datos del libro.
// Si los datos no son válidos, deberá responder 400 e indicarlo.
async function addBook(req, res) {
  try {
    const { titulo, autor } = req.body;

    const schema = Joi.object({
      titulo: Joi.string().required(),
      autor: Joi.string().required(),
    });

    await schema.validateAsync({ titulo, autor });

    const consulta = 'INSERT INTO books (titulo, autor) VALUES (?, ?)';
    const [result] = await database.pool.query(consulta, [titulo, autor]);

    const { insertId } = result;

    const selectQuery = 'SELECT * FROM books WHERE id = ?';
    const [rows] = await database.pool.query(selectQuery, insertId);

    res.status(201);
    res.send(rows[0]);
  } catch (err) {
    handleError(err, res);
  }
}

// UPDATE endpoint updateBook que actualiza todo el contenido de un libro.
// Recibirá el id del libro como request param :id y un body con los datos del libro.
// El endpoint:
// 1. Si el libro no existe, deberá responder 404 e indicarlo.
// 2. Si los datos no son válidos, deberá responder 400 e indicarlo.
// 3. Si el id existe y los datos son válidos, modificar el libro y devolver 200.
async function updateBook(req, res) {
  try {
    const { id } = req.params;
    const { titulo, autor } = req.body;

    let schema = Joi.number().positive().required();
    await schema.validateAsync(id);

    schema = Joi.object({
      titulo: Joi.string().required(),
      autor: Joi.string().required(),
    });

    await schema.validateAsync({ titulo, autor });

    const consulta = 'SELECT * FROM books WHERE id = ?';
    const [rows] = await database.pool.query(consulta, [id]);

    if (!rows || !rows.length) {
      const error = new Error('Libro no encontrado');
      error.code = 404;

      throw error;
    }

    const updateQuery = 'UPDATE books SET autor = ?, titulo = ? WHERE id = ?';
    await database.pool.query(updateQuery, [autor, titulo, id]);

    const selectQuery = 'SELECT * FROM books WHERE id = ?';
    const [newRows] = await database.pool.query(selectQuery, [id]);

    res.send(newRows[0]);
  } catch (err) {
    handleError(err, res);
  }
}

// PATCH endpoint assignBook que asigna un libro a un usuario
// recibirá el id del libro como request param :id
// recibirá el id del usuario en el body de la request,
// es decir: { userId: $idDelUsuario } (habrá que validarlo)
// El endpoint:
// 1. Si el libro no existe, deberá responder 404 e indicarlo.
// 1. Si el usuario no existe, deberá responder 404 e indicarlo.
// 3. Si el libro está prestado a otro usuario deberá responder
// 400 e indicarlo.
// 4. Si el libro existe y está libre, deberá asignarlo al usuario
// que se manda en la request y responder con los datos del libro
// que ha sido modificado.
async function assignBook(req, res) {
  try {

    const { id } = req.params;
    const { userId } = req.body;

    const schema = Joi.number().positive().required();

    await schema.validateAsync(id);
    await schema.validateAsync(userId);

    const [books] = await database.pool.query('SELECT * FROM books WHERE id = ?', id);

    if (!books || !books.length) {
      const err = new Error('Libro no encontrado');
      err.code = 404;

      throw err;
    }

    const [users] = await database.pool.query('SELECT * FROM users WHERE id = ?', userId);

    if (!users || !users.length) {
      const err = new Error('Usuario no encontrado');
      err.code = 404;

      throw err;
    }

    const book = books[0];

    if (book.user_id !== null) {
      const err = new Error('El libro ya estaba asignado');
      err.code = 400;

      throw err;
    }

    const updateQuery = 'UPDATE books SET user_id = ? WHERE id = ?';
    await database.pool.query(updateQuery, [userId, id]);

    const selectQuery = 'SELECT * FROM books WHERE id = ?';
    const [newRows] = await database.pool.query(selectQuery, [id]);

    res.send(newRows[0]);
  } catch (err) {
    handleError(err, res);
  }
}


// PATCH endpoint unassignBook que libera un libro.
// Recibirá el id del libro como request param :id
// El endpoint:
// 1. Si el libro no existe, deberá responder 404 e indicarlo.
// 2. Si el libro ya está libre, deberá responder 400 e indicarlo.
// 3. Si el libro existe y no está libre, deberá desasignarlo
// y responder con los datos del libro que ha sido modificado.
async function unassignBook(req, res) {
  try {
    const { id } = req.params;

    const schema = Joi.number().positive().required();
    await schema.validateAsync(id);

    const [books] = await database.pool.query('SELECT * FROM books WHERE id = ?', id);

    if (!books || !books.length) {
      const err = new Error('Libro no encontrado');
      err.code = 404;

      throw err;
    }

    const book = books[0];

    if (!book.user_id) {
      const err = new Error('El libro ya está libre');
      err.code = 400;

      throw err;
    }

    const updateQuery = 'UPDATE books SET user_id = null WHERE id = ?';
    await database.pool.query(updateQuery, id);

    const selectQuery = 'SELECT * FROM books WHERE id = ?';
    const [newRows] = await database.pool.query(selectQuery, [id]);

    res.send(newRows[0]);
  } catch (err) {
    handleError(err, res);
  }
}

// DELETE endpoint deleteBook que borra un libro.
// Recibirá el id del libro como request param :id
// El endpoint:
// 1. Si el libro no existe, deberá responder 404 e indicarlo.
// 2. Si el libro no está libre, deberá responder 400 e indicarlo.
// 3. Si el libro existe y está libre, deberá borrarlo, poner el
// status code 204 en la response y no devolver ningún contenido en el body.
async function deleteBook(req, res) {
  try {

    const { id } = req.params;

    const schema = Joi.number().positive().required();
    await schema.validateAsync(id);

    const [books] = await database.pool.query('SELECT * FROM books WHERE id = ?', id);

    if (!books || !books.length) {
      const err = new Error('Libro no encontrado');
      err.code = 404;

      throw err;
    }

    const book = books[0];

    if (book.user_id !== null) {

      const [users] = await database.pool.query('SELECT * FROM users WHERE id = ?', book.user_id);
      const user = users[0];

      const err = new Error(`El libro ${book.titulo} está asignado al usuario ${user.nombre}, no se puede borrar`);
      err.code = 400;

      throw err;
    }

    const query = 'DELETE FROM books WHERE id = ?';
    await database.pool.query(query, id);

    res.status(204);
    res.send();

  } catch (err) {
    handleError(err, res);
  }
}

module.exports = {
  getBooks,
  addBook,
  updateBook,
  assignBook,
  unassignBook,
  deleteBook,
};
