const Joi = require('joi');

const { database } = require('../infrastructure');
const { handleError } = require('./utils/error-handler')

async function getUsers(req, res) {
  try {
    const query = 'SELECT * FROM users';
    const resultado = await database.pool.query(query);

    res.send(resultado[0]);
  } catch (err) {
    handleError(err, res);
  }
}

async function getUserById(req, res) {
  try {
    const { id } = req.params;
    const schema = Joi.number().positive();
    await schema.validateAsync(id);
    
    const query = 'SELECT * FROM users WHERE id = ?';
    const [rows] = await database.pool.query(query, id);

    if (!rows || !rows.length) {
      // res.status(404);
      // return res.send({ error: 'Usuario no encontrado' });

      const err = new Error('Usuario no encontrado');
      err.code = 404;

      throw err;
    }
    
    return res.send(rows[0]);

  } catch (err) {
    handleError(err, res);
  }
}

module.exports = {
  getUsers,
  getUserById,
};
