require('dotenv').config();
const express = require('express');

const { booksController, usersController } = require('./controllers');

const app = express();

app.use(express.json());

// Users 
app.get('/api/users', usersController.getUsers);
app.get('/api/users/:id', usersController.getUserById);

// Books
app.get('/api/books', booksController.getBooks);

app.listen(3000, () => console.log('Escuchando'));
