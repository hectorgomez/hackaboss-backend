const Joi = require('joi');

const database = require('../infrastructure/database');

async function getUsers(req, res) {
  try {
    const conexion = await database.getConnection();
    const query = 'SELECT * FROM users';
    const [rows] = await conexion.query(query);
    await conexion.end();

    res.send(rows);
  } catch (err) {
    res.status(500);
    res.send({ err: err.message });
  }
}

// validamos que el id es un número positivo
// enviamos el usuario encontrado al cliente
// devolvemos 404 Not Found si no encontramos el user en base de datos.
async function getUserById(req, res) {
  try {
    const { id } = req.params;
    const schema = Joi.number().positive().required();

    await schema.validateAsync(id);

    const conexion = await database.getConnection();
    const query = 'SELECT * FROM users WHERE id = ?';
    const [rows] = await conexion.query(query, id);

    if (rows && rows.length) {
      res.send(rows[0]);
    } else {
      // res.status(404);
      // res.send({ err: `No se encuentra el usuario con id: ${id}` })
      const err = new Error(`No se encuentra el usuario con id: ${id}`);
      err.code = 404;
      throw err;
    }

    await conexion.end();
  } catch (err) {

    // let status;

    // if (err.isJoi) {
    //   status = 400;
    // } else if (err.code) {
    //   status = err.code;
    // } else {
    //   status = 500;
    // }

    const status = err.isJoi ? 400 : (err.code || 500);
    res.status(status);
    res.send({ err: err.message });
  }
}

module.exports = {
  getUsers,
  getUserById,
};
