const { database } = require('../infrastructure')

async function getBooks(req, res) {
  try {
    const connection = await database.getConnection();
    const query = 'SELECT * FROM books';
    const [books] = await connection.query(query);

    res.send(books);
    
    await connection.end();
  } catch (err) {
    res.status(500);
    res.send({ error: err.message });
  }
}

module.exports = {
  getBooks,
};
