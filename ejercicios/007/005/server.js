// Mismas funcionalidades que el ejercicio 4, pero ahora las rutas
// deberán añadir en el body de la response el valor de req.timestamp.

// Es decir, la response tendrá una forma parecida a:

// {
//   "timestamp": "...",
//   "message": "..."
// }

const express = require('express');

const app = express();

const port = 3000;

app.use((req, res, next) => {
  console.log(`La url es: ${req.url}`);
  req.timestamp = new Date();
  next();
});

app.use((req, res, next) => {
  console.log(req.timestamp);
  next();
});

app.get('/hola', (req, res) => {
  res.send({
    timestamp: req.timestamp,
    message: 'Hola mundo!',
  });
});

app.get('/express', (req, res) => {
  res.send({
    timestamp: req.timestamp,
    message: 'Hola Express!',
  });
});


app.listen(port, () => console.log(`Estoy escuchando el puerto ${port}`));