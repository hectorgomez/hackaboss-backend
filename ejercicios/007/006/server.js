// Servidor que escucha en localhost:3000 y responde con el
// contenido del archivo imagen.jpg cuando se hace una petición
// GET pidiendo el archivo /imagen.jpg

// GET http://localhost:3000/imagen.jpg

const path = require('path');
const express = require('express');

const app = express();
const port = 3000;

const rutaStatic = path.resolve(__dirname, 'static');

app.use(express.static(rutaStatic));

app.listen(port, () => console.log(`Escuchando en ${port}`));
