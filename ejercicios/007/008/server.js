// Servidor que escucha en localhost:3000 y tiene un endpoint
// GET /api/users que acepta name y email como query params.
// Este endpoint devuelve un objeto en la respuesta con la forma
// { name: $name, email: $email } si se envían estos parámetros
// en la request.

// req.query
// /api/users?name=pepito&email=pepito@gmail.com

// {
//   name: 'pepito',
//   email: 'pepito@gmail.com'
// }

// {
//   name: 'PEPITO',
//   email: 'PEPITO@GMAIL.COM'
// }

const express = require('express');
const app = express();

const port = 3000;

app.get('/api/users', (req, res) => {
  const { name, email } = req.query;

  res.send({ name, email });
});

app.listen(port, () => console.log(`Listening to localhost:${port}`));
