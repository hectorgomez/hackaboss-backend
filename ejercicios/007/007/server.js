// Servidor que escucha en localhost:3000 y tiene un endpoint
// GET /api/users/:userId/photos/:id
// Este endpoint devuelve un objeto en la respuesta con la forma
// { user: 34, photo: 67 }

// /users/34/photos/67

const express = require('express');

const app = express();
const port = 3000;

app.get('/api/users/:userId/photos/:id', (req, res) => {
  const { userId, id } = req.params;

  const data = {
    user: Number(userId),
    photo: Number(id),
  };

  res.send(data);
});

app.listen(port, () => console.log(`Escuchando en el puerto ${port}`));
