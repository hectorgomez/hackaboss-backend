// Mismas funcionalidades que el ejercicio 2, pero añadiendo un
// nuevo middleware entre el primer middleware y las rutas.

// Ahora el primer middleware deberá meter en la request la property
// timestamp que contiene la fecha actual, concretamente en req.timestamp.
// El segundo middleware deberá imprimir por pantalla el valor de
// timestamp antes de transferir el control a la ruta correspondiente.

const express = require('express');

const app = express();

const port = 3000;

app.use((req, res, next) => {
  console.log(`La url es: ${req.url}`);
  req.timestamp = new Date();
  next();
});

app.use((req, res, next) => {
  console.log(req.timestamp);
  next();
});

app.get('/hola', (req, res) => {
  res.send({ mensaje: 'Hola mundo!' });
});

app.get('/express', (req, res) => {
  res.send('Hola Express');
});


app.listen(port, () => console.log(`Estoy escuchando el puerto ${port}`));