// Usando el módulo fs de Node, escribe una función que acepte como argumentos dos strings.
// El primer argumento representará un nombre de archivo y el segundo un contenido a escribir en el archivo.

// Llama a esta función para escribir un fichero file.txt en el directorio actual el contenido 'Hola Backend!'

// Puedes usar callbacks o promises, aunque lo recomendable es lo segundo.

const fs = require('fs').promises;

async function escribeFichero(destino, contenido) {
  try {
    await fs.writeFile(destino, contenido);
  } catch (err) {
    console.error('Ha ocurrido un error!');
  }
}

const ruta = 'file.txt';
const data = 'Hola Backend!!!!!!!!';

escribeFichero(ruta, data);
