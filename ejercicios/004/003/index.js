// Usando el módulo fs de NodeJS, escribe una función que acepte como argumento un string.
// A
// La función deberá imprimir el string 'Es un directorio' si el Path que le pasamos es un directorio.
// La función deberá imprimir el string 'Es un archivo' si el Path que le pasamos es un archivo.
// La función deberá imprimir el string 'No existe' si el Path que le pasamos no existe.

// Puedes usar callbacks o promises, aunque lo recomendable es lo segundo.

const fs = require('fs').promises;
const path = require('path');

async function imprimeEstado(destino) {
  try {
    const metadata = await fs.stat(destino);

    if (metadata.isDirectory()) {
      console.log('Es un directorio');
    }
    
    if (metadata.isFile()) {
      console.log('Es un archivo');
    }
  } catch (err) {
    if (err.code === 'ENOENT'){
      console.error('No existe');
    } else {
      throw err;
    }
  }
}

const ruta = path.join(__dirname, 'file.txt');

imprimeEstado(ruta);
