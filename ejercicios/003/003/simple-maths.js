const double = (x) => { 
  const n = Number(x);

  if (!Number.isFinite(n)) {
    throw new Error('Not a number!');
  }

  // if (Number.isNaN(n)) {
  //   throw new Error('Not a number!');
  // }

  return n * 2;
}

module.exports = { double };
