// Modifica el ejercicio anterior para que cuando el argumento que le llega a
// la función `double(x)` del módulo `simple-maths` no sea un número, se tire un error.

// Modifica el módulo `index` para capturar el error y mostrarle al usuario
// el mensaje `El argumento debe ser un número` por pantalla.

// Pista 1: Hay varias formas de comprobar si una variable contiene un número en JS,
// las más fáciles de usar son usar `Number.isFinite(x)` o `Number.isNaN(x)`.

// Pista 2: puedes usar `throw new Error()` para tirar un error, y un
// bloque `try-catch` para capturarlo.

const { double } = require('./simple-maths');

const { argv } = process;

try {
  console.log(double(argv[2]));
} catch (err) {
  console.error('El argumento debe ser un número');
  console.error(err);
}
