const double = x => Number(x) * 2;

module.exports = { double };
