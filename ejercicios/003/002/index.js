// Crea un módulo `simple-maths` que exponga una función `double(x)`,
// que calcula el doble de un número que se le pasa como argumento.

// Crea otro módulo `index` que hace uso del anterior. Este módulo
// usará como entrada el primer argumento que se le pasa a la ejecución
// del módulo, y se lo pasará al módulo `simple-math`, quien calculará el
// doble del número, e `index` se encargará de imprimir el resultado por pantalla.

// Pista: los valores almacenados en `argv` son siempre considerados strings,
// por lo que deberemos hacer la conversión a número con `Number(x)`.

const simpleMaths = require('./simple-maths');

const { argv } = process;

const result = simpleMaths.double(argv[2]);
console.log(result);
