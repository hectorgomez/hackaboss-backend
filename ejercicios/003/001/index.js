// Crea un módulo `index` que simplemente muestre por pantalla 
// la primera palabra que se le pasa como primer argumento 
// cuando se ejecuta este módulo desde bash.

const { argv } = process;

console.log(argv[2]);

// console.log(process.argv[2]);
