USE backend2_restaurants;

ALTER TABLE users ADD role VARCHAR(50) DEFAULT 'user';

INSERT INTO users (name, email, password, role) VALUES
  ('Administrador', 'admin@restaurants-api.com', '$2a$10$rscPDUGTlgUWbJE3pCOxaOQBEspRoMDDk87RoKw4CndlRcUeDCohi', 'admin'); /* adminPass */