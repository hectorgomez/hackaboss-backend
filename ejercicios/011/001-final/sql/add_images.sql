USE backend2_restaurants;

CREATE TABLE `images` (
  `id` int NOT NULL AUTO_INCREMENT,
  `review_id` int NOT NULL,
  `user_id` int NOT NULL,
  `url` varchar(1023) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (review_id)
    REFERENCES review(id)
    ON DELETE CASCADE,
  FOREIGN KEY (user_id)
    REFERENCES users(id)
    ON DELETE CASCADE
);
