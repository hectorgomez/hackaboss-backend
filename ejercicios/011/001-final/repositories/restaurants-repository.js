const { database } = require('../infrastructure');

async function findRestaurants() {
  const [restaurants] = await database.pool.query('SELECT * FROM restaurants');

  return restaurants;
}

async function findRestaurantById(id) {
  const query = 'SELECT * FROM restaurants WHERE id = ?';
  const [restaurants] = await database.pool.query(query, id);

  return restaurants && restaurants[0];
}

async function deleteRestaurant(id) {
  const query = 'DELETE FROM restaurants WHERE id = ?';

  return database.pool.query(query, id);
}

module.exports = {
  findRestaurants,
  findRestaurantById,
  deleteRestaurant,
};
