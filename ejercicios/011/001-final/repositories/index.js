const imagesRepository = require('./images-repository');
const restaurantsRepository = require('./restaurants-repository');
const reviewsRepository = require('./reviews-repository');
const usersRepository = require('./users-repository');

module.exports = {
  imagesRepository,
  restaurantsRepository,
  reviewsRepository,
  usersRepository,
};
