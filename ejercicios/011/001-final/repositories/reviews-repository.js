const { database } = require('../infrastructure');

async function findReviewsByUserId(userId, sortfield, sortdir) {
  let query = 'SELECT * FROM review WHERE user_id = ?';

  if (sortfield && sortdir) {
    query += ` ORDER BY ${sortfield} ${sortdir}`;
  }

  const [reviews] = await database.pool.query(query, userId);
  
  return reviews;
}

async function findReviews() {
  const query = 'SELECT * FROM review';
  const [reviews] = await database.pool.query(query);
  
  return reviews;
}

async function findReviewsWithImages() {
  const reviews = await findReviews();

  const getUrls = async (reviewId) => {
    const query = 'SELECT * FROM images WHERE review_id = ?';
    const [images] = await database.pool.query(query, reviewId);

    return images.map(i => ({
      id: i.id,
      url: i.url,
    }));
  }

  return Promise.all(reviews.map(async review => ({
    ...review,
    images: await getUrls(review.id),
  })));
}

async function findReviewById(id) {
  const query = 'SELECT * FROM review WHERE id = ?';
  const [reviews] = await database.pool.query(query, id);

  return reviews && reviews[0];
}

async function createReview(data, userId) {
  const query = 'INSERT INTO review (user_id, restaurant_id, rating, text) VALUES (?, ?, ?, ?)';

  const [result] = await database.pool.query(query, [
    userId,
    data.restaurantId,
    data.rating,
    data.text
  ]);

  return findReviewById(result.insertId);
}

async function updateReviewRatingAndText(id, rating, text) {
  const query = 'UPDATE review SET rating = ?, text = ? WHERE id = ?';
  await database.pool.query(query, [rating, text, id]);

  return findReviewById(id);
}

async function deleteReviewById(id) {
  const query = 'DELETE FROM review WHERE id = ?';

  return database.pool.query(query, id);
}

module.exports = {
  findReviews,
  findReviewsWithImages,
  findReviewsByUserId,
  createReview,
  findReviewById,
  updateReviewRatingAndText,
  deleteReviewById,
};
