const { database } = require('../infrastructure');

async function findImageById(id) {
  const query = 'SELECT * FROM images WHERE id = ?';
  const [images] = await database.pool.query(query, id);

  return images && images[0];
}

async function createImage(url, reviewId, userId) {
  const query = 'INSERT INTO images (review_id, user_id, url) VALUES (?, ?, ?)';
  const [result] = await database.pool.query(query, [reviewId, userId, url]);

  return findImageById(result.insertId);
}

async function deleteImageById(id) {
  const query = 'DELETE FROM images WHERE id = ?';

  return database.pool.query(query, id);
}

module.exports = {
  deleteImageById,
  findImageById,
  createImage,
};
