const Joi = require('joi');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const { usersRepository } = require('../repositories');

async function getUsers(req, res, next) {
  try {
    const { role } = req.auth;

    if (role !== 'admin') {
      const err = new Error('Sólo los admins pueden listar usuarios');
      err.status = 403;
      throw err;
    }

    const users = await usersRepository.getUsers();
    res.send(users);
  } catch (err) {
    next(err);
  }
}

async function createUser(req, res, next) {
  try {
    const { role } = req.auth;

    if (role !== 'admin') {
      const err = new Error('Sólo los admins pueden crear usuarios');
      err.status = 403;
      throw err;
    }

    const { name, email, password } = req.body;

    const userSchema = Joi.object({
      name: Joi.string(),
      email: Joi.string().email().required(),
      password: Joi.string().min(5).max(20).required(),
    });

    await userSchema.validateAsync({ name, email, password });
    const user = await usersRepository.getUserByEmail(email);

    if (user) {
      const err = new Error('Ya existe un usuario con ese email');
      err.status = 409;
      throw err;
    }

    const passwordHash = await bcrypt.hash(password, 10);

    const createdUser = await usersRepository.createUser({
      role: 'user',
      name,
      email,
      passwordHash,
    });

    res.send(createdUser);

  } catch (err) {
    next(err);
  }
}

async function login(req, res, next) {
  try {
    const { email, password } = req.body;

    const schema = Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().min(5).max(20).required(),
    });

    await schema.validateAsync({ email, password });

    // 1. Recuperamos el usuario desde la base de datos.

    const user = await usersRepository.getUserByEmail(email);

    if (!user) {
      const error = new Error('No existe el usuario');
      error.code = 404;
      throw error;
    }

    // 2. Comprobamos que el password que nos están enviando es válido.

    const isValidPassword = await bcrypt.compare(password, user.password);

    if (!isValidPassword) {
      const error = new Error('El password no es válido');
      error.code = 401;
      throw error;
    }

    // 3. Construimos el JWT para enviárselo al cliente.
    const tokenPayload = { id: user.id, role: user.role };

    const token = jwt.sign(
      tokenPayload,
      process.env.JWT_SECRET,
      { expiresIn: '30d' },
    );
    
    res.send({
      userId: user.id,
      token,
    });

  } catch (err) {
    next(err);
  }
}

async function register(req, res, next) {
  try {
    const {
      name,
      email,
      password,
      repeatedPassword,
    } = req.body;

    const registerSchema = Joi.object({
      name: Joi.string(),
      email: Joi.string().email().required(),
      password: Joi.string().min(5).max(20).required(),
      repeatedPassword: Joi.string().min(5).max(20).required(),
    });

    await registerSchema.validateAsync(req.body);

    if (password !== repeatedPassword) {
      const err = new Error('Los password no coinciden');
      err.status = 400;
      throw err;
    }

    const user = await usersRepository.getUserByEmail(email);

    if (user) {
      const err = new Error('Ya existe un usuario con ese email');
      err.status = 409;
      throw err;
    }

    const passwordHash = await bcrypt.hash(password, 10);
    
    const createdUser = await usersRepository.createUser({
      role: 'user',
      name,
      email,
      passwordHash,
    });

    res.status(201);
    res.send(createdUser);

  } catch (err) {
    next(err);
  }
}

async function deleteUser(req, res, next) {
  try {
    const { role } = req.auth;
    const { id } = req.params;

    if (role !== 'admin') {
      const err = new Error('Sólo los admins pueden borrar usuarios');
      err.status = 403;
      throw err;
    }

    const user = await usersRepository.getUserById(id);

    if (!user) {
      const err = new Error('No existe el usuario');
      err.status = 404;
      throw err;
    }

    await usersRepository.deleteUser(id);
    res.status(204);
    res.send();
  } catch (err) {
    next(err);
  }
}

module.exports = {
  createUser,
  getUsers,
  login,
  register,
  deleteUser,
};