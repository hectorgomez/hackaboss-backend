const ImagesController = require('./images-controller');
const RestaurantsController = require('./restaurants-controller');
const ReviewsController = require('./reviews-controller');
const UsersController = require('./users-controller');

module.exports = {
  ImagesController,
  RestaurantsController,
  ReviewsController,
  UsersController,
};
