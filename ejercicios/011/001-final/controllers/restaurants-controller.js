const { database } = require('../infrastructure');
const { restaurantsRepository } = require('../repositories');

async function getRestaurants(req, res, next) {
  try {
    const restaurants = await restaurantsRepository.findRestaurants();
    res.send(restaurants);
  } catch (err) {
    next(err);
  }
}

/**
 * Función que calcula la puntuación de un restaurante. El id del
 * restaurante vendrá como request param, es decir, vendrá en
 * req.params.restaurantId
 * 
 * Habrá que calcular la media aritmética de las puntuaciones (ratings)
 * de todas las reviews del restaurante. Y devolver un objeto al cliente
 * con la forma { name: $nombreRestaurante, rating: $mediaPuntuaciones }
 */
async function getScore(req, res, next) {
  try {
    const { id } = req.params;
    const restaurant = await restaurantsRepository.findRestaurantById(id);

    if (!restaurant) {
      const err = new Error('Restaurante no encontrado');
      err.status = 404;
      throw err;
    }

    const reviewsQuery = 'SELECT * FROM review WHERE restaurant_id = ?';
    const [reviews] = await database.pool.query(reviewsQuery, id);
    
    if (!reviews.length) {
      res.status(204);
      res.send();
    } else {
      const avgRating = (reviews.reduce((sum, review) => sum + review.rating, 0)) / reviews.length;

      res.send({
        name: restaurant.name,
        rating: avgRating.toFixed(4),
      });
    }

  } catch (err) {
    next(err);
  }
}

async function deleteRestaurant(req, res, next) {
  try {
    const { role } = req.auth;
    const { id } = req.params;

    if (role !== 'admin') {
      const err = new Error('Sólo los admins pueden borrar usuarios');
      err.status = 403;
      throw err;
    }

    const restaurant = await restaurantsRepository.findRestaurantById(id);

    if (!restaurant) {
      const err = new Error('No existe el restaurante');
      err.status = 404;
      throw err;
    }

    await restaurantsRepository.deleteRestaurant(id);
    res.status(204);
    res.send();

  } catch (err) {
    next(err);
  }
}

module.exports = {
  getRestaurants,
  getScore,
  deleteRestaurant,
};
