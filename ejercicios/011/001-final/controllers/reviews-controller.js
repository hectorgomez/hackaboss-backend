const Joi = require('joi');

const {
  imagesRepository,
  restaurantsRepository,
  reviewsRepository,
} = require('../repositories');

/**
 * Función que crea una review para el usuario que viene en
 * el token JWT. Es decir, el user_id que guardaremos en 
 * la base de datos para la review creada, será el id que venga
 * en req.auth.id
 */
async function createReview(req, res, next) {
  try {
    const { restaurantId } = req.params;
    const { id } = req.auth;
    const { rating, text } = req.body;

    const schema = Joi.object({
      rating: Joi.number().min(1).max(5).required(),
      text: Joi.string().min(1).max(500),
    });

    await schema.validateAsync({ rating, text });

    const restaurant = await restaurantsRepository.findRestaurantById(restaurantId);

    if (!restaurant) {
      const err = new Error('El restaurante no existe');
      err.status = 404;

      throw err;
    }

    const reviews = await reviewsRepository.findReviewsByUserId(id);
    const existsReviewForRestaurant = reviews.some(r => r.restaurant_id === Number(restaurantId));

    if (existsReviewForRestaurant) {
      const err = new Error('El restaurante ya ha sido valorado por el usuario');
      err.status = 409;
      
      throw err;
    }

    const createdReview = await reviewsRepository.createReview({ restaurantId, rating, text }, id);
    res.status(201);
    res.send(createdReview);
  } catch (err) {
    next(err);
  }
}

/**
 * Función que busca las reviews que corresponden al usuario
 * que se pasa como parámetro en una request param, es decir,
 * el id del usuario estará en req.params.userId
 * 
 * Deberá comprobar que el id del usuario del cual se piden
 * las reviews es igual al id del usuario que viene en el 
 * token JWT. Es decir, el valor de req.auth.id
 * 
 * Si no coinciden, devolverá 401 Unauthorized 
 */
async function getReviewsByUserId(req, res, next) {
  try {
    const { userId } = req.params;
    const { sortfield, sortdir } = req.query;

    const sortSchema = Joi.object({
      sortfield: Joi.string().valid('rating'),
      sortdir: Joi.string().valid('asc', 'desc'),
    });

    await sortSchema.validateAsync({ sortfield, sortdir });
    
    if (Number(userId) !== req.auth.id) {
      const err = new Error('El usuario no tiene permisos');
      err.status = 403;
      throw err;
    }

    const reviews = await reviewsRepository.findReviewsByUserId(userId, sortfield, sortdir);    
    res.send(reviews);
  } catch (err) {
    next(err);
  }
}

async function getReviews(req, res, next) {
  try {
    const { role } = req.auth;

    if (role !== 'admin') {
      const err = new Error('Sólo los admins pueden listar todas las reviews');
      err.status = 403;
      throw err;
    }

    const reviews = await reviewsRepository.findReviewsWithImages();
    res.send(reviews);
  } catch (err) {
    next(err);
  }
}

/**
 * Función que modifica una review. El id de la review vendrá
 * como request param, es decir, vendrá en req.params.reviewId
 * 
 * Habrá que comprobar que la review que se intenta modificar
 * pertenece al usuario que viene en el token JWT. Es decir, el
 * user_id de la review en base de datos coincide con req.auth.id
 */
async function updateReview(req, res, next) {
  try {
    const { id } = req.params;
    const { rating, text } = req.body;

    const schema = Joi.object({
      rating: Joi.number().integer().min(1).max(5).required(),
      text: Joi.string(),
    });

    await schema.validateAsync({ rating, text });

    const review = await reviewsRepository.findReviewById(id);

    if (!review) {
      const err = new Error('La review no existe');
      err.status = 404;
      throw err;
    }

    // WITHOUT ADMIN:
    // if (review.user_id !== req.auth.id) {
    //   const err = new Error('El usuario no tiene permisos');
    //   err.status = 403;
    //   throw err;
    // }

    // WITH ADMIN:
    if ((review.user_id !== req.auth.id) && (req.auth.role !== 'admin')) {
      const err = new Error('El usuario no tiene permisos');
      err.status = 403;
      throw err;
    }

    const updatedReview = await reviewsRepository
      .updateReviewRatingAndText(reviewId, rating, text);

    res.send(updatedReview);
  } catch (err) {
    next(err);
  }
}

async function addReviewImage(req, res, next) {
  try {
    const { file } = req;
    const { id } = req.params;

    const review = await reviewsRepository.findReviewById(id);

    if (!review) {
      const err = new Error('La review no existe');
      err.status = 404;
      throw err;
    }

    const url = `static/images/${id}/${file.filename}`;
    const image = await imagesRepository.createImage(url, id, review.user_id);

    res.status(201);
    res.send(image);

  } catch (err) {
    next(err);
  }
}

async function deleteReviewById(req, res, next) {
  try {
    const { id } = req.params;
    const { id: userId, role } = req.auth;

    const review = await reviewsRepository.findReviewById(id);

    if (!review) {
      const err = new Error('No existe la review');
      err.code = 404;

      throw err;
    }

    if (userId !== review.user_id && role !== 'admin') {
      const err = new Error('Sin permisos, sólo el dueño de la review o el admin puede borrar');
      err.code = 403;

      throw err;
    }

    await reviewsRepository.deleteReviewById(id);

    res.status(204);
    res.send();
  } catch (err) {
    next(err);
  }
}


module.exports = {
  createReview,
  getReviewsByUserId,
  updateReview,
  getReviews,
  addReviewImage,
  deleteReviewById,
};
