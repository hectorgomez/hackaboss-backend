require('dotenv').config();
const path = require('path');
const fs = require('fs');
const express = require('express');
const multer = require('multer');
const { v4: uuidv4 } = require('uuid');

const {
  ImagesController,
  RestaurantsController,
  ReviewsController,
  UsersController,
} = require('./controllers');

const { validateAuthorization } = require('./middlewares');

const { PORT } = process.env;

const staticPath = path.resolve(__dirname, 'static');

const app = express();
app.use(express.json());
app.use(express.static(staticPath));

const upload = multer({
  storage: multer.diskStorage({
    destination: function (req, file, cb) {
      const { id } = req.params;
      const folder = path.join(__dirname, `static/images/${id}/`);
      fs.mkdirSync(folder, { recursive: true });

      cb(null, folder)
    },
    filename: function (req, file, cb) {
      cb(null, uuidv4() + path.extname(file.originalname))
    }
  }),
  limits: {
    fileSize: 1024 * 1024, // 1 MB
  }
});

// Endpoints / Rutas

// Restaurants
app.get('/api/restaurants', RestaurantsController.getRestaurants);
app.get('/api/restaurants/:id/score', RestaurantsController.getScore);
app.delete('/api/restaurants/:id', validateAuthorization, RestaurantsController.deleteRestaurant);

// Users
app.get('/api/users', validateAuthorization, UsersController.getUsers);
app.post('/api/users', validateAuthorization, UsersController.createUser);
app.post('/api/users/login', UsersController.login);
app.post('/api/users/register', UsersController.register);
app.delete('/api/users/:id', validateAuthorization, UsersController.deleteUser);

// Reviews
app.get('/api/reviews', validateAuthorization, ReviewsController.getReviews);
app.get('/api/reviews/:userId', validateAuthorization, ReviewsController.getReviewsByUserId);
app.post('/api/reviews/:restaurantId', validateAuthorization, ReviewsController.createReview);
app.put('/api/reviews/:id', validateAuthorization, ReviewsController.updateReview);
app.delete('/api/reviews/:id', validateAuthorization, ReviewsController.deleteReviewById);

app.post(
  '/api/reviews/:id/images',
  validateAuthorization,
  upload.single('image'),
  ReviewsController.addReviewImage,
);

// Images
app.delete('/api/images/:id', validateAuthorization, ImagesController.deleteImageById);

app.use(async (err, req, res, next) => {
  const status = err.isJoi ? 400 : (err.status || 500);
  res.status(status);
  res.send({ resultado: 'ERROR', error: err.message });
});

// Escuchar un puerto
app.listen(PORT, () => console.log(`Escuchando en el puerto ${PORT}`));
