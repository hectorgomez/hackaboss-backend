CREATE DATABASE IF NOT EXISTS backend2_restaurants;

USE backend2_restaurants;

CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `restaurants` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `review` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `restaurant_id` int NOT NULL,
  `rating` int NOT NULL,
  `text` varchar(511) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (user_id)
    REFERENCES users(id)
    ON DELETE CASCADE,
  FOREIGN KEY (restaurant_id)
    REFERENCES restaurants(id)
    ON DELETE CASCADE
);

INSERT INTO restaurants (name, address) VALUES 
 ("Pizzeria Roma", "Calle Torre, 12"),
 ("Cosa Nostra", "Calle Trabajo, 20"),
 ("Bar Clementín", "Plaza Cubela, número 13"),
 ("Mesón Rodríguez", "Plaza de Ourense, 96"),
 ("Pulpeira Valdeorras", "Calle Santander número 11");

INSERT INTO users (name, email, password) VALUES
  ('Alicia Álvarez', 'alicia@restaurants.com', '$2a$10$pz6yMDCzdI.y6XOskKXQHO.aGVwsPxmoQOCK/ZWZyDCuKCVtOcGiu'), /* pass1 */
  ('Jorge Romero', 'jorge@restaurants.com', '$2a$10$Vpyx4Z4TStFuyulsS.GSaeUMkORMetNJpgcJVUKGqgAOOr1x6lzdu'), /* pass2 */
  ('Verónica Díaz', 'veronica@restaurants.com', '$2a$10$Pe2Jx8xtMCMGbxGTBnoeZuOf.BHRRpg0xE1x6uan.0gNfIxie3QfS'); /* pass3 */
