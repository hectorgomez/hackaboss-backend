const { database } = require('../infrastructure');

// const database = require('../infrastructure/database');

async function getRestaurants() {
  const [restaurants] = await database.pool.query('SELECT * FROM restaurants');

  return restaurants;
}

async function findRestaurantById(id) {
  const query = 'SELECT * FROM restaurants WHERE id = ?';
  const [restaurants] = await database.pool.query(query, id);

  return restaurants[0];
}

module.exports = {
  getRestaurants,
  findRestaurantById,
};
