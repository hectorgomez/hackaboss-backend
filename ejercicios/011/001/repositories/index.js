const restaurantsRepository = require('./restaurants-repository');
const usersRepository = require('./users-repository');
const reviewsRepository = require('./reviews-repository');

module.exports = {
  restaurantsRepository,
  usersRepository,
  reviewsRepository,
};
