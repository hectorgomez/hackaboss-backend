const { database } = require('../infrastructure');

async function findReviewById(id) {
  const query = 'SELECT * FROM review WHERE id = ?';
  const [reviews] = await database.pool.query(query, id);

  return reviews[0];
}

async function addReview(data) {
  const query = 'INSERT INTO review (user_id, restaurant_id, rating, text) VALUES (?, ?, ?, ?)';
  const [result] = await database.pool.query(query, [data.userId, data.restaurantId, data.rating, data.text]);

  return findReviewById(result.insertId);
}

async function findReviewsByUserId(userId) {
  const query = 'SELECT * FROM review WHERE user_id = ?';
  const [reviews] = await database.pool.query(query, userId);

  return reviews;
}

module.exports = {
  addReview,
  findReviewsByUserId,
};
