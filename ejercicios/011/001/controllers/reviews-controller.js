const Joi = require('joi');
const { reviewsRepository, restaurantsRepository, usersRepository } = require('../repositories');

async function createReview(req, res, next) {
  try {
    const { restaurantId } = req.params;
    const { id } = req.auth;
    const { rating, text } = req.body;

    const schema = Joi.object({
      rating: Joi.number().min(1).max(5).required(),
      text: Joi.string().max(511),
    });

    await schema.validateAsync({ rating, text });

    const restaurant = await restaurantsRepository.findRestaurantById(restaurantId);

    if (!restaurant) {
      const err = new Error('El restaurante no existe');
      err.code = 404;

      throw err;
    }

    const data = {
      userId: id,
      restaurantId,
      rating,
      text,
    };
    
    const review = await reviewsRepository.addReview(data);

    res.status(201);
    res.send(review);
  } catch (err) {
    next(err);
  }
}

async function getReviewsByUserId(req, res, next) {
  try {
    const { userId } = req.params;
    const { id } = req.auth;

    if (Number(userId) !== id) {
      const err = new Error('Sin permisos');
      err.code = 401;

      throw err;
    }

    const reviews = await reviewsRepository.findReviewsByUserId(userId);

    res.send(reviews);
  } catch (err) {
    next(err);
  }
}

module.exports = {
  createReview,
  getReviewsByUserId,
};
