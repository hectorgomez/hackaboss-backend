const usersController = require('./users-controller');
const restaurantsController = require('./restaurants-controller');
const reviewsController = require('./reviews-controller');

module.exports = {
  usersController,
  restaurantsController,
  reviewsController,
};
