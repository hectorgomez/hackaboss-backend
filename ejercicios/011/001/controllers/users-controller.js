const Joi = require('joi');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

// const usersRepository = require('../repositories/users-repository');

const { usersRepository } = require('../repositories');

async function register(req, res, next) {
  try {
    
    const {
      name,
      email,
      password,
      repeatedPassword,
    } = req.body;

    const schema = Joi.object({
      name: Joi.string(),
      email: Joi.string().email().required(),
      password: Joi.string().min(5).max(20).required(),
      repeatedPassword: Joi.string().min(5).max(20).required(),
    });

    await schema.validateAsync({
      name,
      email,
      password,
      repeatedPassword,
    });

    // await schema.validateAsync(req.body);

    if (password !== repeatedPassword) {
      const err = new Error('Password y repeatedPassword deben coincidir');
      err.code = 400;

      throw err;
    }

    const user = await usersRepository.findUserByEmail(email);

    if (user) {
      const err = new Error(`Ya existe un usuario con email: ${email}`);
      err.code = 409;

      throw err;
    }

    const passwordHash = await bcrypt.hash(password, 10);

    const createdUser = await usersRepository.createUser({
      name,
      email,
      password: passwordHash,
    });

    res.status(201);
    res.send({
      id: createdUser.id,
      name: createdUser.name,
      email: createdUser.email,
    });

  } catch (err) {
    next(err);
  }
}

async function login(req, res, next) {
  try {
    const { email, password } = req.body;

    const schema = Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().min(5).max(20).required(),
    });

    await schema.validateAsync({ email, password });

    const user = await usersRepository.findUserByEmail(email);

    if (!user) {
      const error = new Error('No existe el usuario');
      error.code = 401;

      throw error;
    }

    const isValidPassword = await bcrypt.compare(password, user.password);

    if (!isValidPassword) {
      const error = new Error('El password no es válido');
      error.code = 401;

      throw error;
    }

    const tokenPayload = {
      id: user.id,
      role: user.role,
    };

    const token = jwt.sign(
      tokenPayload,
      process.env.SECRET,
      { expiresIn: '1d' },
    );
    
    res.send({
      id: user.id,
      token,
    });

  } catch (err) {
    next(err);
  }
}

async function getUsers(req, res, next) {
  try {
    const { role } = req.auth;

    if (role !== 'admin') {
      const err = new Error('Solo los admins pueden listar todos los usuarios');
      err.code = 403;

      throw err;
    }

    const users = await usersRepository.getUsers();

    res.send(users);
  } catch (err) {
    next(err);
  }
}

module.exports = {
  register,
  login,
  getUsers,
};
