const restaurantsRepository = require('../repositories/restaurants-repository');

async function getRestaurants(req, res, next) {
  try {
    const restaurants = await restaurantsRepository.getRestaurants();
    res.send(restaurants);
  } catch (err) {
    next(err);
  }
}

module.exports = {
  getRestaurants,
};
