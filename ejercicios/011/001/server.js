require('dotenv').config();
const express = require('express');

const {
  restaurantsController,
  usersController,
  reviewsController,
} = require('./controllers');

const { validateAuthorization } = require('./middlewares/validate-auth');

// const restaurantsController = require('./controllers/restaurants-controller');
// const usersController = require('./controllers/users-controller');

const { PORT } = process.env;

const app = express();

app.use(express.json());


// Users:
app.post('/api/users/register', usersController.register);
app.post('/api/users/login', usersController.login);
app.get('/api/users', validateAuthorization, usersController.getUsers);

// Restaurants:
app.get('/api/restaurants', restaurantsController.getRestaurants);

// Reviews:
app.post('/api/reviews/:restaurantId', validateAuthorization, reviewsController.createReview);
app.get('/api/reviews/:userId', validateAuthorization, reviewsController.getReviewsByUserId);

app.use(async (err, req, res, next) => {
  const status = err.isJoi ? 400 : (err.code || 500);
  res.status(status);
  res.send({ error: err.message });
});

app.listen(PORT, () => console.log(`Restaurant-API listening at port ${PORT}`));
