// Servidor web que escucha cualquier request.
// Cuando la request es un POST a /data, se devuelve el JSON recibido.
// Cuando es otra request cualquiera, se responde 404 Not Found sin body.

const http = require('http');

const server = http.createServer();

const hostname = 'localhost'
const port = 3000;

server.on('request', (req, res) => {
  let body = [];

  req.on('data', dato => body.push(dato));
  req.on('error', () => console.error('Ha habido un error'));

  req.on('end', () => {
    body = Buffer.concat(body).toString();

    if (req.method === 'POST' && req.url === '/data') {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.end(body);
    } else {
      res.statusCode = 404;
      res.end();
    }
  });

});

server.listen(port, hostname, () => {
  console.log(`Servidor corriendo en ${hostname}:${port}`);
});
