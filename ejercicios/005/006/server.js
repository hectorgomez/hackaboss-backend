// Servidor web que gestiona objetos del tipo 
// { email: pepito@gmail.com, message: 'Hola soy Pepito' }

// Internamente usará un fichero database.json donde se almacenarán datos.
// Este archivo se sitúa en el directorio /database dentro de la raiz del servidor.

// Cuando la request es un GET a /api/messages, se responde al cliente con
// todos los datos que hay en el JSON de almacenamiento.

// Cuando la request es un POST a /api/messages, se incluye el objeto
// recibido en el JSON de almacenamiento, y se responde al cliente con
// todos los datos que hay en el JSON de almacenamiento.

// Cuando es otra request cualquiera, se responde 404 Not Found sin body.

const http = require('http');
const fs = require('fs').promises;
const path = require('path');

const server = http.createServer();
const databaseFile = 'database/database.json';

const hostname = 'localhost';
const port = 3000;

server.on('request', (req, res) => {
  let body = [];

  req.on('data', (dato) => body.push(dato));
  req.on('error', () => console.error('Ha habido un error'));

  req.on('end', async () => {
    body = Buffer.concat(body).toString();

    const { method, url } = req;

    if (method === 'GET' && url === '/api/messages') {
      const ruta = path.join(__dirname, databaseFile);
      const data = await fs.readFile(ruta, 'utf8');

      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.end(data);

    } else if (method == 'POST' && url === '/api/messages') {
      const newMessage = JSON.parse(body);
      const ruta = path.join(__dirname, databaseFile);
      const data = await fs.readFile(ruta, 'utf8');

      const dataObject = JSON.parse(data);
      
      dataObject.messages.push(newMessage);
      await fs.writeFile(ruta, JSON.stringify(dataObject));

      res.statusCode = 201;
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(dataObject));
    } else {
      res.statusCode = 404;
      res.end();
    }
  });
});

server.listen(port, hostname, () => console.log(`Escuchando en ${hostname}:${port}`));