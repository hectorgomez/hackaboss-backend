// Servidor web que escucha cualquier request y devuelve el objeto
// { curso: 'backend' } cuando se llama al la ruta /curso
// devuelve { message: 'hello world' } cuando se llama /message
// y devuelve { message: 'No lo encuentro!' } con status 404 en otro caso

const http = require('http');

const server = http.createServer();

const hostname = 'localhost'
const port = 3000;

server.on('request', (req, res) => {
  res.setHeader('Content-Type', 'application/json');

  if (req.url === '/curso') {
    res.statusCode = 200;
    res.end(JSON.stringify({ curso: 'backend' }));
  } else if (req.url === '/message') {
    res.statusCode = 200;
    res.end(JSON.stringify({ message: 'hello world' }));
  } else {
    res.statusCode = 404;
    res.end(JSON.stringify({ message: 'No lo encuentro' }));
  }
});

server.listen(port, hostname, () => console.log(`Server running at ${hostname}:${port}`));
