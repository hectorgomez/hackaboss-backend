// Servidor web que escucha cualquier request y devuelve el objeto
// { curso: 'backend' } cuando se llama al la ruta /curso
// devuelve { message: 'hello world' } cuando se llama /message
// y devuelve { message: 'No lo encuentro!' } con status 404 en otro caso

// Además, el servidor imprime por pantalla el método y la URL de cada request.

const http = require('http');

const server = http.createServer();

const hostname = 'localhost'
const port = 3000;

// function logRequest(req) {} 

const logRequest = (req) => {
  // console.log(req.method);
  // console.log(req.url);
  const { method, url } = req;
  console.log(`[${new Date()}] Request de tipo ${method} a la URL ${url}`);
}

server.on('request', (req, res) => {
  logRequest(req); 

  res.setHeader('Content-Type', 'application/json');

  if (req.url === '/curso') {
    res.statusCode = 200;
    res.end(JSON.stringify({ curso: 'backend' }));
  } else if (req.url === '/message') {
    res.statusCode = 200;
    res.end(JSON.stringify({ message: 'hello world' }));
  } else {
    res.statusCode = 404;
    res.end(JSON.stringify({ message: 'No lo encuentro' }));
  }
});

server.listen(port, hostname, () => {
  console.log(`Servidor corriendo en ${hostname}:${port}`);
});
