# Validación de datos
Los datos que llegan a un servidor web por lo general necesitan un proceso de validación. Por ejemplo: comprobar que el valor del campo email es efectivamente un email, que la password no sea menor de 4 caracteres o que si recibimos un campo que represente un año sea numérico.

Una medida sencilla para validar datos sería comprobaer que todos los valores se ajustan a unos criterios y si es así seguimos ejecutando la función correspondiente y si no devolvemos un error al usuario. Es posible realizar la validación de esos datos manualmente usando (múltiples) condicionales de Javascript, pero esa tarea de validación puede ser tediosa para validaciones complejas. Para resolver ese problema de manera sencilla existe [joi](https://www.npmjs.com/package/joi):

```
npm install joi
```

```javascript
const Joi = require('joi');
```

El funcionamiento de **Joi** es muy simple, primero creamos un *schema* o patrón y le pasamos valores para comprobar que se ajustan a ese patrón.

El ejemplo más básico sería un schema que valida cualquier cosa:

```javascript
const Joi = require('joi');

const schema = Joi.any();

const resultadoValidacion = schema.validate('Hola');
console.log(resultadoValidacion); // esto imprimirá { value: 'Hola' }
```

```javascript
const Joi = require('joi');

const schema = Joi.string();

const resultadoValidacion = schema.validate(23);
console.log(resultadoValidacion); // esto imprimirá un objeto con una propiedad error con información sobre el error
```

Es decir, la función `schema.validate` permite comprobar si el valor que se le pasa se adecúa al patrón que hay definido para el `schema`.

## Validando objetos
Por ejemplo si queremos validar un que un objeto tiene unas determinadas propiedades de unos determinados tipos:

```javascript
const Joi = require('joi');

const schema = Joi.object({ id: Joi.number(), nombreArticulo: Joi.string() });

const resultadoValidacion = schema.validate({ id: 'tres', nombreArticulo: 'vaso cristal' });
console.log(resultadoValidacion); // esto imprimirá un objeto con una propiedad error con información sobre el error
const resultadoSegundaValidacion = schema.validate({ id: 4, nombreArticulo: 'vaso cristal' });
console.log(resultadoSegundaValidacion) // esto imprimirá { value: { id: 4, nombreArticulo: 'vaso cristal' } }
```

Por defecto las propiedades de un objeto son opcionales para Joi. Podemos indicar que una propiedad es obligatoria usando la función required() , lo que hará que no permita valores undefined (pero si null):

```javascript
const Joi = require('joi');

const schema = Joi.object({ id: Joi.number().required(), nombre: Joi.string() });

const resultado = schema.validate({ nombre: 'Hector Gomez' })
console.log(resultado) // esto imprimirá un objeto con una propiedad error con información sobre el error
const resultado2 = schema.validate({ id: 23, nombre: 'Hector Gomez' })
console.log(resultado2); // esto imprimirá { value: { id: 23, nombre: 'Hector Gomez' } }
```

Otros métodos importantes:

```
.valid(a, b, c): hará que sólo sean válidos los valores a, b o c.
.invalid(a, b, c): hará que los valores a, b y c siempre sean inválidos.
.allow(a, b, c): hará que los valores a, b o c sean válidos siempre independientemente de otras restricciones.
```

```javascript
const Joi = require('joi');

const schema = Joi.object({ nombre: Joi.string().valid('hector', 'pedro', 'javier')});

const resultadoValidacion = schema.validate({ nombre: 'ramiro' });
console.log(resultadoValidacion); // esto imprimirá un objeto con una propiedad error con información sobre el error
const resultadoSegundaValidacion = schema.validate({ nombre: 'pedro' });
console.log(resultadoSegundaValidacion) // esto imprimirá { value: { nombre: 'pedro' } }
```

## Validando strings
Para validar strings tenemos también algunas funciones útiles:

```
Longitud de la cadena:
Joi.string().min(10): valida solo strings con unha longitud mínima de 10 caracteres.
Joi.string().max(10): lo mismo pero para longitud máxima.
Joi.string().length(10): solo valida strings con una longitud de 10 caracteres.
Contenido de la cadena:
Joi.string().email(): valida solo emails.
Joi.string().ip(): valida solo direcciones IP.
Joi.string().uri(): valida solo URLs.
Joi.string().creditCard(): valida números de tarjetas de crédito.
Formato de la cadena:
Joi.string().lowercase(): valida solo textos en minúsculas.
Joi.string().uppercase(): lo mismo pero en mayúsculas.
```

```javascript
const Joi = require('joi');

const schema = Joi.object({ nombre: Joi.string().min(7)});

const resultadoValidacion = schema.validate({ nombre: 'ramiro' });
console.log(resultadoValidacion); // esto imprimirá un objeto con una propiedad error con información sobre el error
const resultadoSegundaValidacion = schema.validate({ nombre: 'eustaquio' });
console.log(resultadoSegundaValidacion) // esto imprimirá { value: { nombre: 'eustaquio' } }
```

## Validando números
Para validar números usamos `Joi.number()` complementado con los siguientes métodos:

```
Joi.number().min(value): value sería el número mínimo.
Joi.number().max(value): value sería el número máximo.
Joi.number().precision(n): n sería el máximo de decimales admintidos.
Joi.number().positive(): solo acepta números positivos. También existe el método .negative().
Joi.number().integer(): solo acepta números enteros.
```

```javascript
const Joi = require('joi');

const schema = Joi.number().positive().min(40);

const resultadoValidacion = schema.validate(-21);
console.log(resultadoValidacion); // esto imprimirá un objeto con una propiedad error con información sobre el error
const resultadoSegundaValidacion = schema.validate(38);
console.log(resultadoSegundaValidacion); // esto imprimirá un objeto con una propiedad error con información sobre el error
const resultadoTerceraValidacion = schema.validate(45);
console.log(resultadoTerceraValidacion); // esto imprimirá { value: 45 }
```

Validando fechas
Para validar fechas usamos Joi.date() que tiene los siguientes métodos.

```
Joi.date().min(date): date sería la fecha mínima.
Joi.date().max(date): date sería la fecha máxima.
Joi.date().timestamp(): aceptaría solo unix time.
```

```javascript
const Joi = require('joi');

const schema = Joi.date().less('2020-01-01');

const ahora = new Date();

const resultadoValidacion = schema.validate(ahora);
console.log(resultadoValidacion); // esto imprimirá un objeto con una propiedad error con información sobre el error
const resultadoSegundaValidacion = schema.validate(new Date('2019-08-05'));
console.log(resultadoSegundaValidacion) // esto imprimirá { value: 2019-08-05T00:00:00.000Z }
```

## Validando requests
Usando **Joi** en nuestros endpoints para validar los datos de las requests. Podemos usar **Joi** para validar los datos que nos entran en las requests a nuestro servidor Express/NodeJS.

Para ello, podemos usar una sentencia `if` en nuestros endpoints que compruebe el resultado de la validación, si ese resultado contiene un error, es decir, si contiene una propiedad `error`, entonces podemos elegir notificar al usuario en la response de que no se ha podido realizar la tarea debido a que los datos de entrada eran incorrectos. Por ejemplo, en este caso, podemos enviar al usuario un código de error `400 BAD REQUEST`. Ejemplo:

```javascript
const express = require('express');
const Joi = require('joi');

const app = express();

app.use(express.json());

app.post('/articulos', (req, res) => {
  const schema = Joi.object({
    nombre: Joi.string(),
    referencia: Joi.number().required(),
  });

  const validation = schema.validate(req.body);

  if (validation.error) {
    res.status(400);
    res.send({ error: validation.error });
  } else {
    res.send(req.body);
  }
});

app.listen(3000);
```

## Validación asíncrona
Como hemos visto, las funciones del módulo `fs`, y otras funciones que tienen que ver con el acceso a bases de datos (y que veremos próximamente), tienen versiones síncronas y asíncronas. Por lo general, como hemos dicho, es preferible trabajar con las versiones asíncronas ya que tienen mejor rendimiento. Pues bien, en **Joi** también tenemos una versión asíncrona de la función `validate`, que como es esperable se llama `validateAsync`. Esta será la versión que usaremos siempre que podamos, dentro de un bloque `try-catch`. Ejemplo:

```javascript
const express = require('express');
const Joi = require('joi');

const app = express();

app.use(express.json());

async function createUser(req, res) {
  try {
    const { email, password } = req.body; // obtenemos datos del objeto request

    const schema = Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().min(6).max(20).required(),
    });

    await schema.validateAsync({ email, password }); // validamos

    res.send({ resultado: 'OK' });
  } catch (err) {
    const status = err.isJoi ? 400 : 500;
    res.status(status); // configuramos status code
    res.send({ error: err.message }); // devolvemos mensaje de error al cliente
  }
}

app.post('/api/users', createUser);

app.listen(3000);
```

Nótese que cuando **Joi** tira un error, éste tiene una propiedad `message` que indica la naturaleza del error, y una propiedad `isJoi` a `true`, con lo que en nuestro catch podremos devolver un `400 Bad Request`, por ejemplo, si el error proviene de **Joi**, es decir, es un error de validación de datos.

---

## Bonus track: uuid
A la hora de introducir nuevos datos en un sistema, podemos experimentar lo que se denominan `colisiones`. Una colisión se produce cuando dos entidades del mismo tipo se identificar de la misma manera, por ejemplo, tienen el mismo ID. En el ámbito de las bases de datos es común que a cada una de las filas o documentos que se les asigne un identificador único, que pude ser númerico en el caso de las bases de datos SQL o alfanumérico en otros tipos de bases de datos. Por simplicidad, en muchas ocasiones, delegamos en la base de datos el identificar los datos de entrada de forma única, dejando que ésta establezca un ID para el objeto entrante en el sistema. Sin embargo, en algunas ocasiones no disponemos de una base de datos, o tenemos un sistema que utiliza varios almacenes de datos, o incluso que guarda datos en una cola a la espera de ser procesados por otro sistema en el futuro.

En estos últimos escenarios, se hace imprescindible tener alguna herramienta para identificar de forma única cada objeto. Una de las formas más utilizadas es asignar a cada objeto un `uuid` (*universally unique identifier*, *identificador único universal*). Aunque existen diferentes versiones del algoritmo para generar un `uuid`, durante el curso usaremos la version 4, que se usa para generar `uuid` aleatorios. En Node, podemos usar el paquete de *npm* [uuid](https://www.npmjs.com/package/uuid):

```
npm i uuid
```

```javascript
const { v4: uuidv4 } = require('uuid');
uuidv4(); // ⇨ '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed'
```

Usando este paquete, podremos generar identificadores únicos, que está garantizado que no tendrán colisiones. Podemos usar estos identificadores identificar objetos, archivos en disco, etc.

---

### Ejercicio
* Servidor que escucha en localhost:3000 y tiene un endpoint `POST /api/users/:userId` que acepta un body con la estructura:
```javascript
{
  email: 'email@dominio.com',
  premios: 14
}
```
* Este endpoint debe validar:
- El userId que se pasa como *path param* es obligatorio y es un numero mayor que 0.
- El email es obligatorio y está bien formado.
- El valor de premios es un número entre 0 y 100 y es opcional.

* En caso de error, debería devolver el status `400 BAD REQUEST` al cliente y como body un *JSON* con la estructura:
```json
{
  "resultado": "ERROR",
  "error": "$mensajeDeErrorDeJoi"
}
```
* Si no hay errores de validación debería devolver el status `201 CREATED` al cliente y como body un *JSON* con la estructura:
```json
{
  "resultado": "OK"
}
```

---

## Proyecto 1
### Parte 1:
* Servidor que escucha en `localhost:3000` y tiene las rutas:
- `/GET /api/users/:name`: devuelve el usuario con `nombre === name` o `404 NOT FOUND` si no existe ningún usuario con ese nombre.
- `/GET /api/users`: devuelve todos los users si no se manda `edad` en el *querystring*.
- `/GET /api/users`: si nos llega el *querystring* edad, devolvemos los users que tengan esa edad.

* Pista: `Array.find` y `Array.filter` permiten filtrar y encontrar elementos en un array que cumplen una condición.

### Parte 2:
* Crea una nueva colección en *Postman* que se llame `express-users-api` y que contenga las requests que hemos creado en la parte 1.
* Añade un nuevo endpoint que acepte peticiones `POST` en `/api/users` con un body
```javascript
{
  nombre: $nombre,
  edad: $edad,
}
```
* Este endpoint debe añadir la info entrante a nuestro `users.json` y nos devuelva el usuario creado con un status code `201 CREATED`.
* Usa Joi para verificar que las request entrantes tienen el formato correcto. Ambas propiedades (nombre, edad) son obligatorias. Edad tiene que ser mayor que 18 años.

### Parte 3:
* Crea un nuevo endpoint que acepte peticiones `POST` a `/api/users/:name/images` donde `:name` hace referencia a uno de los usuarios que tenemos en users.json y cuyo body tenga content-type `multipart/form-data` y contenga una imagen.
* Si el usuario existe, guardaremos la imagen en el directorio `/images`, y devolvemos `201 CREATED`, con el objeto
```javascript
{
  resultado: 'OK',
  imagen: $nombreDelEnElServidor
}
```
* Si no existe, devolveremos un error 404 Not Found.

---

## Proyecto 2
* Copia el código del proyecto 1, pero haz algunas modificaciones:
- El nombre del archivo que se guarda debe ser la fecha actual en unix-time + extensión del archivo original.
- Comenta la parte donde se usa el unix-time para generar el nombre y ahora genéralo con un `uuid` + extensión del archivo original.
- El parámetro `port` que representa el puerto en el que se levanta el servidor se introduce por variable de entorno; usa `dotenv` para configurar esa variable.