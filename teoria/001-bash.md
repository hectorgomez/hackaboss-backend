# Bash
## Primeros pasos
**Bash** es un interprete de comandos, o lo que es lo mismo, un `shell`. Existen otros intérpretes de comandos, pero tal vez `bash` es el más extendido y uno de los más amigables para el usuario.

### Algunas consideraciones sobre Bash

* Puede usarse para ejecutar comandos o para automatizar tareas usando scripts.
* Es *case-sensitive* (distingue mayúsculas y minúsculas).
* Permite moverse por el sistema de ficheros (pwd, cd, ls...)
* Permite manipular ficheros y directorios (mkdir, cp, mv, rm...)
* Permite consultar su propio historial de comandos usando `history`.

### Primeros comandos
* Consultar el directorio actual con `pwd`
* Cambiar el directorio actual con  `cd`
* Copiar ficheros o directorios con `cp`
* Listar archivos y directorio con  `ls`
* Crear nuevos directorios con `mkdir`
* Copiar ficheros o directorios con `cp`
* Mover o renombrar ficheros y directorios con `mv`
* Borrar ficheros o directorios con `rm`

---
### Argumentos de un comando

 Los comandos están compuestos por el nombre del comando y una serie de argumentos. Los argumentos que no empiezan por guión son argumentos de entrada, por ejemplo:

```
cp test.txt final.txt
```

En ese caso cp es el comando y recibe dos argumentos de entrada: `test.txt` y `final.txt`. Lo que hará será copiar el fichero `test.txt` en `final.txt`, creándolo si no existe.

Si los argumentos empiezan por un guión determinan una serie de opciones del comando: 

```
ls -l -a -h
```

que también se pueden combinar en uno sólo, por lo tanto el comando anterior sería equivalente que 

```
ls -lah
```

Ese comando listaría los ficheros del directorio actual, para ver qué hace cada argumento podemos ejecutar `man ${comando}` en este caso

```
man ls
```

Además de `man`, podemos usar `tldr` (en inglés: *too long; didn't read*) para ver los usos y una breve descripción sobre la utilidad de un comando en particular. `tldr` no está instalado por defecto en la mayoría de distribuciones Linux, ni en MacOS, por lo que para usarlo deberemos instalarlo previamente usando un gestor de paquetes. Por ejemplo, en Debian y derivados (Ubuntu, Mint, etc.) podemos usar `apt`, y en MacOS podemos usar `brew`.

---
## Variables de entorno
Cuando cualquier proceso se ejecuta en nuestra máquina, el sistema operativo, además de un espacio de memoria y otra serie de recursos de hardware, le asigna un *entorno*. Este *entorno* puede definirse como las variables a las que el proceso tiene acceso y puede consultar durante su ejecución. **Bash** no es un una excepción. Podemos consultar el entorno de ejecución en **Bash** con el sencillo comando:

```
env
```

Este entorno es crítico en el desarrollo de software, ya que muchas veces se usa para indicarle a un proceso una manera concreta de comportarse. Por ejemplo, podríamos indicarle (como haremos durante el curso) a un servidor web, cuál número de puerto que debe usar durante su ejecución; también podríamos indicarle cuál es la dirección de otro servicio que el servidor necesita (por ejemplo, su base de datos), etc.

---
## Manejo de ficheros
**Bash** permite realizar muchas operaciones básicas con ficheros.

### Operaciones de lectura de ficheros
* Imprimir por pantalla todo el contenido de un fichero con `cat`
* Scroll sobre el contenido de un fichero con `less`
* Imprimir por pantalla las primeras líneas del contenido de un fichero con `head`
* Imprimir por pantalla las últimas líneas del contenido de un fichero con `tail`
* Imprimir por pantalla el número de líneas, palabras y caracteres de un fichero con `wc`
### Operaciones de escritura de ficheros
* Crear un fichero nuevo vacío con `touch`
* Editar un fichero con `nano`, `vim` o `emacs`
### Operaraciones de búsqueda
* Hacer búsquedas en el sistema de ficheros con `find`
* Hacer búsquedas sobre el contenido de un fichero con `grep`

---
## Redirecciones y pipes
* La salida de los comandos (output) de bash puede redirigirse. Ejemplo:
```
ls -lha > list.txt
```

* El output de un comando puede ser input de otro. Ejemplo:
```
ls -lha | wc -l 
```

---
### Ejercicio 1
1. Crea un nuevo directorio llamado `helloFolder`
2. Sitúate en ese directorio, y crea un archivo vacío llamado `helloFile`.
3. Ahora redirige la salida de `env` (variables de entorno del proceso `bash`) a ese archivo.
4. Imprime todo el contenido de ese archivo por pantalla.
5. Lo mismo que en el punto anterior pero filtrando las líneas que contengan la letra `N`.
6. Sal del directorio situándote en su directorio padre, y borra `helloFolder` con todo su contenido.

### Ejercicio 2
1. Muestra por pantalla las rutas de todos los ficheros que tienes con la extensión `.js` en tu carpeta de usuario.
2. Repite la operación, pero en lugar de mostrarlo por pantalla, vuelca esa información en un archivo que se llame `my-js-files`
3. Averigua, en base al último resultado, cuantos archivos tienes con la extensión `.js` en carpeta de usuario.
4. Elimina el archivo que has creado.

### Ejercicio 3

1. Imagina que no recuerdas cúal era el comando que usaste en el punto 2.1, usa `history` para verlo. Imagina que es un comando que usaste hace unas horas, por lo que sólo recuerdas que usaste `find` pero no recuerdas los argumentos concretos. Usa `history` para averiguar qué comando usaste, filtrando la salida de `history` de tal forma que sólo muestre resultados que contengan la palabra `find`.
