# Arquitectura de aplicaciones Node.js
Aunque existen múltiples paradigmas (no necesariamente excluyentes entre ellos) y principios de arquitectura backend ([SOLID](https://es.wikipedia.org/wiki/SOLID), [MVC](https://es.wikipedia.org/wiki/Modelo%E2%80%93vista%E2%80%93controlador), [Hexagonal Architecture](https://en.wikipedia.org/wiki/Hexagonal_architecture_(software)), [DDD](https://en.wikipedia.org/wiki/Domain-driven_design), [CQRS](https://martinfowler.com/bliki/CQRS.html), etc.), durante el curso sólo veremos un par de buenas prácticas básicas que comparten todas ellas. 

## Controladores
A la hora de desarrollar aplicaciones web, en lugar de tener un único archivo como hemos visto hasta ahora con todo el código, lo ideal es separar responsabilidades. Una forma de hacerlo es distribuir las rutas de la aplicación en diferentes grupos, por ejemplo agrupando por el prefijo de la ruta. Así, tenemos el concepto de **controlador** que no sería más que una agrupación de uno o varios *endpoints*.

Por ejemplo, si tenemos algunos *endpoints* en la aplicación que tienen el prefijo `/api/users`, y otros que tienen el prefijo `/api/photos` podríamos agrupar los primeros en un módulo que se llamase `UsersController` y los segundos en otro módulo que se llamase `PhotosController`. Cada uno de estos módulos sería el responsable de exponer las funciones que manejan sus rutas, y en el punto de entrada de la aplicación las importaríamos y configuraríamos estas funciones como handlers para cada ruta.

```javascript
const express = require('express');

// requerimos los controllers
const { photosController, usersController } = require('./controllers');

const app = express();

app.use(express.json());

// Configuración de las rutas
app.get('/users', usersController.getUsers);
app.get('/users/:id', usersController.getUserById);
app.get('/photos', photosController.getPhotos);

app.listen(4000, () => console.log('Escuchando'));
```

Es decir, para que esto funcione correctamente deberíamos tener un archivo `/controllers/users-controller.js` que expusiese las funciones `getUsers` y `getUserById`, y un archivo `/controllers/photos-controller.js` que expusiese la función `getPhotos`:

```javascript
// fichero /controllers/users-controller.js

async function getUsers(req, res) {
  // codigo
}

async function getUserById(req, res) {
  // codigo
}

module.exports = { getUsers, getUserById };
```

```javascript
// fichero /controllers/photos-controller.js

async function getPhotos(req, res) {
  // codigo
}

module.exports = { getPhotos };
```

## Repositorios de datos
A su vez, es común que una determinada operación la hagamos en diferentes *controllers*, es decir, que tengamos que realizarla para responder a varias peticiones distintas. Por ejemplo podríamos querer recuperar la información del usuario en múltiples peticiones de éste a nuestro backend.

Para factorizar esos accesos, y así poder reusarlos, usamos lo que normalmente se denomina **repositorios**. Los *repositorios* no son más que agrupaciones lógicas de funciones que normalmente trabajan con el mismo origen de datos. Siguiendo el ejemplo anterior de recuperación de información del usuario, podemos tener un `UserRepository` que agrupará todas las funciones que lean o escriban información de los usuarios a nuestro origen de datos (que durante el curso será una base de datos **SQL**).

```javascript
async function findUsers() {}
async function findUserById(id) {}
async function findUserByName(name) {}
async function addUser(data) {}
async function updateUser(data) {}
async function deleteUserById(id) {}

module.exports = {
  findUsers,
  findUserById,
  findUserByName,
  addUser,
  updateUser,
  deleteUserById,
};
```

Así por ejemplo podríamos usar la función `findUserById` en múltiples controllers sin necesidad de escribir su código en todos ellos, favoreciendo la consistencia y la reusabilidad de nuestro código: [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself).

Separar nuestro software en capas tiene otra ventaja: unas capas no dependen de las otras y sus implementaciones son intercambiables. Por ejemplo hemos visto al principio de este tema que existen diferentes tipos de bases de datos. Si por ejemplo en algún momento queremos migrar nuestros datos desde nuestra base de datos *MySQL* a otra base de datos *NoSQL*, sólo tendríamos que adaptar nuestros repositorios. Nuestros controllers seguirían funcionando igual porque el acceso a datos lo tenemos aislado en la capa de repositorios.

---

## Proyecto 1
* Inicializa un nuevo servidor NodeJS usando Express, que se integre con una base de datos MySQL corriendo en la máquina local. Para ello, se proporciona un archivo `database.js` que será el módulo encargado de establecer una conexión con la base de datos. Este archivo usa varias variables de entorno, que estableceremos usando `dotenv`, así pues, en nuestro archivo de entrada del servidor, deberemos requerir `dotenv` y establecer las rutas.
* Para gestionar estas rutas, vamos a hacer uso de controladores específicos. Tendremos un `UsersController` y un `BooksController`. 
* `UsersController` tendrá dos endpoints: uno que devolverá todos los usuarios presentes en la tabla `users` de la base de datos, y otro que devolverá un usuario en base a su id. 
* `BooksController` tendrá un único endpoint que devolverá todos los libros presentes en la tabla `books` de la base de datos. 

---

## Proyecto 2
* Este proyecto es muy parecido al Proyecto 1, pero en este caso vamos a usar `pooling` para gestionar los accesos a la base de datos.
* Además, añadiremos rutas para crear, actualizar y borrar libros.
* También añadiremos rutas para asignar un libro a un usuario, y para desasignar el libro al usuario.
* También crearemos una función que se encarga de manejar los errores y la llamaremos en los bloques catch, para no tener que reescribir código.

---

## Proyecto 2
* Este proyecto es muy parecido al Proyecto 2, pero en este caso vamos a usar *repositorios* para factorizar los accesos a la base de datos y separar responsabilidades.
* También crearemos un error handler y lo añadiremos como último middleware en express.

---