const fs = require('fs').promises;

async function readFile(filename) {
  const data = await fs.readFile(filename, 'utf8');

  return console.log(data.toString());
}

readFile('file.txt');