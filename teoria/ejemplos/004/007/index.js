const fs = require('fs').promises;

async function myFunction() {
  try {
    const data = await fs.readFile('provincias.json', 'utf-8');
    console.log(data);
  } catch (err) {
    console.error(err);
  }
}

myFunction();