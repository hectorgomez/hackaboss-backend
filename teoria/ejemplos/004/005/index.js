const fs = require('fs').promises;

async function myFunction() {
  try {
    const data = await fs.stat('file.txt', 'utf-8');
    console.log(data);
  } catch (err) {
    console.error(err);
  }
}

myFunction();