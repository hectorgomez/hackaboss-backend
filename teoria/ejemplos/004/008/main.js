const fs = require('fs').promises;

const myFunction = async () => {
  const dimensions = { height: 300, width: 200 };

  try {
    await fs.writeFile('dimensions.json', JSON.stringify(dimensions))
  } catch (err) {
    console.error(err);
  }
}

myFunction();