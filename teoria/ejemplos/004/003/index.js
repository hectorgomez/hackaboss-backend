const fs = require('fs').promises;

async function readFile(filename) {
  try {
    const data = await fs.readFile(filename);

    return console.log(data.toString());
  } catch (err) {
    console.error(`error reading file ${filename}`);
  }
}

readFile('fil3.txt'); // fil3.txt no existe en el sistema de ficheros