const express = require('express');
const multer  = require('multer');
 
const app = express();

const multerOptions = {
  dest: 'uploads/',
  limits: {
    fileSize: 52428800, // 50 MB
  }
};

const upload = multer(multerOptions);
 
app.post('/api/profile', upload.single('avatar'), (req, res, next) => {
  // el valor de req.file será el contenido del archivo `avatar`
  // el valor de req.body será el resto de valores del formulario
  const { file, body } = req;
  res.send({ file, body });
});

app.listen(3000);