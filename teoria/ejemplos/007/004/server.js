const express = require('express');
const app = express();

const handler = (req, res, next) => {
  console.log('Función middleware 1');
  next();
}

const handler2 = (req, res, next) => {
  console.log('Función middleware 2');
  next();
}

const handler3 = (req, res) => {
  console.log('Función middleware 3');
  res.send();
}

app.get('/api/ejemplo', handler, handler2, handler3);

app.listen(3000);