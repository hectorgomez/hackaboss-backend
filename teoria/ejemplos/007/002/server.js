const express = require('express');
const morgan = require('morgan');

const app = express();

app.use(morgan('short'));

// Rutas del servidor Express, las vemos en el siguiente apartado
app.get('/api/saludos/mundo', (req, res) => {
  res.send({ mensaje: 'Hola mundo!' });
});

app.get('/api/saludos/express', (req, res) => {
  res.send('Hola Express');
});

app.listen(3000, () => console.log('Escuchando!'));