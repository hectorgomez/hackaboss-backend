const path = require('path');
const express = require('express');

const app = express();

const port = 3000;

const staticPath = path.resolve(__dirname, 'static');

app.use(express.static(staticPath));
// app.use(express.static('static'));

app.listen(port, () => console.log(`Escuchando en ${port}`));