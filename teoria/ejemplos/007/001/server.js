const express = require('express');
const app = express();

app.use((req, res, next) => {
  next(); //pasamos al siguiente middleware
});

app.use((req, res, next) => {
  if (req.headers.authorization === 'superSecreto') {
    next();
  } else {
    res.statusCode = 401;
    res.end('Identificación incorrecta');
  }
});

app.use((req, res) => {
  res.statusCode = 200;
  res.end('Bienvenido!');
});

app.listen(3000, () => console.log('Servidor arriba!'));