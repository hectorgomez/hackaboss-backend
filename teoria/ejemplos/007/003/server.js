const express = require('express');
const app = express();

app.get('/', (req, res) => {
  res.statusCode = 200;
  res.end('Portada!');
});

app.get('/contact', (req, res) => {
 res.statusCode = 200;
 res.end('Contacto');
});

// Sólo se ejecuta si no entra en ninguna de las rutas anteriores
app.use((req, res) => {
 res.statusCode = 404;
 res.end('Not found :(');
});

app.listen(3000);