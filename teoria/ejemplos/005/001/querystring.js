const querystring = require('querystring');

const str = 'foo=bar&abc=xyz&abc=123';
const data = querystring.parse(str);

console.log(data); // imprime un objeto JS
console.log(querystring.stringify(data)); // imprime el string re-procesado