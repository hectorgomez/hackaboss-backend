const http = require('http');

const { PORT } = process.env;

const server = http.createServer();

server.on('request', (req, res) => {
  const { method, url, headers } = req;
  
  console.log(method);
  console.log(url);

  let body = [];
  req.on('data', chunk => body.push(chunk));
  req.on('error', () => console.error('Ha habido un error'));

  req.on('end', () => {
    res.statusCode = 200; // indicamos status code
    res.setHeader('Content-Type', 'application/json'); // indicamos content-type header
    res.end(Buffer.concat(body)); // enviamos body
  });
});

server.listen(PORT, () => console.log(`Server running at ${PORT}`));