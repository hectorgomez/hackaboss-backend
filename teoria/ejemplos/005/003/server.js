const http = require('http');

const server = http.createServer();

server.on('request', (req, res) => {
  const { method, url, headers } = req;
  
  console.log(method);
  console.log(url);

  let body = [];
  req.on('data', chunk => body.push(chunk)); // van llegando los datos de la request
  req.on('error', () => console.error('Ha habido un error')); // en caso de error procesando la request
  req.on('end', () => console.log(Buffer.concat(body).toString())); // todos los datos han llegado

  res.end('Hello!');
});

server.listen(3000);