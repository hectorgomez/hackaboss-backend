const http = require('http');

const server = http.createServer();

const port = 3000;

server.on('request', (req, res) => {
  const { method, url, headers } = req;
  
  console.log(method);
  console.log(url);
  console.log(headers);

  res.end('Hello!');
});

server.listen(port, () => console.log(`Escuchando en el puerto ${port}`));