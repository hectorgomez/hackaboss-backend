const http = require('http');
const server = http.createServer();

const hostname = 'localhost'
const port = 3000;

server.on('request', (req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');

  if (req.url === '/curso') {
    res.end(JSON.stringify({ curso: 'backend' }));
  } else {
    res.end(JSON.stringify({ message: 'hola node' }));
  }
});

server.listen(port, hostname, () => console.log(`Server running at ${hostname}:${port}`));