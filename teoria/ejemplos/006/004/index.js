const { add, sub, format } = require('date-fns');

const fecha = new Date();
console.log(fecha); // 2021-04-05T15:36:37.559Z

const fecha2 = add(fecha, { months: 5, days: 4 });
console.log(fecha2); // 2021-06-09T15:36:37.559Z

const fecha3 = sub(fecha2, { years: 5 });
console.log(fecha3); // 2016-06-09T15:36:37.559Z

const fecha3Formateada = format(fecha3, "do/MM/yo");
console.log(fecha3Formateada); // 09/06/2016