const dotenv = require('dotenv');

dotenv.config();

console.log(process.env.VARIABLE1); // imprime foo
console.log(process.env.VARIABLE2); // imprime bar
console.log(process.env.VARIABLE3); // imprime baz