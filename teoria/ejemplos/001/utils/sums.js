const arraySum = arr => arr.filter(i => i > 3).reduce((sum, i) => sum + i);

module.exports = { arraySum };