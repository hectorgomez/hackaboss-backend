const express = require('express');
const Joi = require('joi');

const app = express();

app.use(express.json());

async function createUser(req, res) {
  try {
    const { email, password } = req.body; // obtenemos datos del objeto request

    const schema = Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().min(6).max(20).required(),
    });

    await schema.validateAsync({ email, password }); // validamos

    // llamada a bd

    res.send({ resultado: 'OK' });
  } catch (err) {
    const status = err.isJoi ? 400 : 500;

    // let status;

    // if (err.isJoi) {
    //   status = 400
    // } else {
    //   status = 500
    // }

    res.status(status); // configuramos status code
    res.send({ error: err.message }); // devolvemos mensaje de error al cliente
  }
}

app.post('/api/users', createUser);

app.listen(3000);