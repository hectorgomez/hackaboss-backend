const express = require('express');
const Joi = require('joi');

const app = express();

app.use(express.json());

app.post('/articulos', (req, res) => {
  const schema = Joi.object({
    nombre: Joi.string(),
    referencia: Joi.number().strict().required(),
  });

  const validation = schema.validate(req.body);

  if (validation.error) {
    res.status(400);
    res.send({ error: validation.error });
  } else {
    res.send(req.body);
  }
});

app.listen(3000);